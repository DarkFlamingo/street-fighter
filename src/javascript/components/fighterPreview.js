import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  const arena = document.querySelector('.preview-container___root');
  arena.innerHTML = '';
  if (fighter) {
    fillFighterDiv(fighter, fighterElement);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function fillFighterDiv(fighter, fighterElement) {
  fighterElement.append(createFighterLogo(fighter));
  const el = createElement({ tagName: 'div', className: 'fighter-preview-information' });
  el.append(createFighterLabel('Name', fighter.name));
  el.append(createFighterLabel('Health', fighter.health));
  el.append(createFighterLabel('Attack', fighter.attack));
  el.append(createFighterLabel('Defense', fighter.defense));
  fighterElement.append(el);
}

function createFighterLabel(title, value) {
  let label = createElement({ tagName: 'label', className: 'fighter-preview-information-label' });
  label.innerText = `${title} : ${value}`;
  return label;
}

function createFighterLogo(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___logo',
    attributes,
  });

  return imgElement;
}
