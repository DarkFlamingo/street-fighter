import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const root = document.getElementById('root');
    const healthBar = document.getElementsByClassName('arena___health-bar');

    const firstFighterCopy = Object.assign(
      { inBlock: false, healthBar: healthBar[0], superHitTime: null },
      firstFighter
    );
    const secondFighterCopy = Object.assign(
      { inBlock: false, healthBar: healthBar[1], superHitTime: null },
      secondFighter
    );
    let buttons = [];
    document.addEventListener('keydown', keyDownCallBack);
    document.addEventListener('keyup', keyUpCallBack);

    function keyDownCallBack(event) {
      buttons.push(event.code);
      const checkArray = buttons.slice(-3);
      switch (event.code) {
        case controls.PlayerOneAttack:
          if (!firstFighterCopy.inBlock) {
            playerOneAttack();
          }
          break;
        case controls.PlayerTwoAttack:
          if (!secondFighterCopy.inBlock) {
            playerTwoAttack();
          }
          break;
        case controls.PlayerOneBlock:
          if (!firstFighterCopy.inBlock) {
            playerOneBlock();
          }
          break;
        case controls.PlayerTwoBlock:
          if (!secondFighterCopy.inBlock) {
            playerTwoBlock();
          }
          break;
      }
      if (
        checkArray.includes(controls.PlayerOneCriticalHitCombination[0]) &&
        checkArray.includes(controls.PlayerOneCriticalHitCombination[1]) &&
        checkArray.includes(controls.PlayerOneCriticalHitCombination[2])
      ) {
        playerOneSuperHit();
      }
      if (
        checkArray.includes(controls.PlayerTwoCriticalHitCombination[0]) &&
        checkArray.includes(controls.PlayerTwoCriticalHitCombination[1]) &&
        checkArray.includes(controls.PlayerTwoCriticalHitCombination[2])
      ) {
        playerTwoSuperHit();
      }
    }

    function keyUpCallBack(event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighterCopy.inBlock = false;
          hideBlock('left');
          break;
        case controls.PlayerTwoBlock:
          secondFighterCopy.inBlock = false;
          hideBlock('right');
          break;
      }
    }

    function playerOneAttack() {
      if (!secondFighterCopy.inBlock) {
        const damage = getDamage(secondFighterCopy, firstFighterCopy);
        if (damage > 0) {
          secondFighterCopy.health -= damage;
          showCommonHit('right');
          checkHealth(secondFighter.health, secondFighterCopy, firstFighterCopy);
        } else {
          showUptake('right');
        }
      }
    }

    function playerTwoAttack() {
      if (!firstFighterCopy.inBlock) {
        const damage = getDamage(secondFighterCopy, firstFighterCopy);
        if (damage > 0) {
          firstFighterCopy.health -= damage;
          showCommonHit('left');
          checkHealth(firstFighter.health, firstFighterCopy, secondFighterCopy);
        } else {
          showUptake('left');
        }
      }
    }

    function playerOneBlock() {
      firstFighterCopy.inBlock = true;
      showBlock('left');
    }

    function playerTwoBlock() {
      secondFighterCopy.inBlock = true;
      showBlock('right');
    }

    function playerOneSuperHit() {
      if (isHitAvaible(firstFighterCopy)) {
        const damage = 2 * firstFighterCopy.attack;
        secondFighterCopy.health -= damage;
        showSuperHit('right');
        checkHealth(secondFighter.health, secondFighterCopy, firstFighterCopy);
        console.log('First Fighter Super Hit');
      }
    }

    function playerTwoSuperHit() {
      if (isHitAvaible(secondFighterCopy)) {
        const damage = 2 * secondFighterCopy.attack;
        firstFighterCopy.health -= damage;
        showSuperHit('left');
        checkHealth(firstFighter.health, firstFighterCopy, secondFighterCopy);
        console.log('Second Fighter Super Hit');
      }
    }

    function checkHealth(originalHealth, defender, attacker) {
      if (defender.health <= 0) {
        defender.healthBar.style.width = 0;

        document.removeEventListener('keydown', keyDownCallBack);
        document.removeEventListener('keyup', keyUpCallBack);

        resolve(attacker);
      } else {
        defender.healthBar.style.width = (defender.health / originalHealth) * 100 + '%';
      }
    }

    function isHitAvaible(fighter) {
      const time = Date.now();
      if (time - fighter.superHitTime > 10000) {
        fighter.superHitTime = time;
        return true;
      }
      return false;
    }

    function showSuperHit(position) {
      const el = document.querySelector(`.arena___${position}-fighter`);
      const attributes = {
        src: '../../../resources/fight/superHit.png',
        title: 'Boom',
        alt: 'Boom',
        id: `arena-super-hit-${position}`,
      };
      const img = createElement({
        tagName: 'img',
        attributes,
      });
      el.append(img);
      setTimeout(() => document.getElementById(`arena-super-hit-${position}`).remove(), 300);
    }

    function showCommonHit(position) {
      const el = document.querySelector(`.arena___${position}-fighter`);
      const attributes = {
        src: '../../../resources/fight/commonHit.png',
        title: 'Hit',
        alt: 'Hit',
        id: `arena-common-hit-${position}`,
      };
      const img = createElement({
        tagName: 'img',
        attributes,
      });
      el.append(img);
      setTimeout(() => document.getElementById(`arena-common-hit-${position}`).remove(), 50);
    }

    function showUptake(position) {
      const el = document.querySelector(`.arena___${position}-fighter`);
      const attributes = {
        src: '../../../resources/fight/uptake.png',
        title: 'Uptake',
        alt: 'Uptake',
        id: `arena-common-hit-${position}`,
      };
      const img = createElement({
        tagName: 'img',
        attributes,
      });
      el.append(img);
      setTimeout(() => document.getElementById(`arena-common-hit-${position}`).remove(), 50);
    }

    function showBlock(position) {
      const el = document.querySelector(`.arena___${position}-fighter`);
      const attributes = {
        src: '../../../resources/fight/block.png',
        title: 'Block',
        alt: 'Block',
        id: `arena-block-${position}`,
      };
      const img = createElement({
        tagName: 'img',
        attributes,
      });
      el.append(img);
    }

    function hideBlock(position) {
      document.getElementById(`arena-block-${position}`).remove();
    }
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1, 2);
  return fighter.defense * dodgeChance;
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}
