import { showModal } from './modal';
import App from '../../app';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const attributes = {
    src: fighter.source,
    title: fighter.name,
    alt: fighter.name,
  };
  const bodyElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___logo',
    attributes,
  });
  showModal({
    title: `Winner : ${fighter.name}`,
    bodyElement,
    onClose: () => {
      document.getElementById('root').innerHTML = '';
      new App();
    },
  });
}
